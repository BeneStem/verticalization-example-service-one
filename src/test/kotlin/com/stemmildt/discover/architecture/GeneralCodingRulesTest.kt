package com.stemmildt.discover.architecture

import com.tngtech.archunit.core.importer.ImportOption.DoNotIncludeArchives
import com.tngtech.archunit.core.importer.ImportOption.DoNotIncludeTests
import com.tngtech.archunit.junit.AnalyzeClasses
import com.tngtech.archunit.junit.ArchTest
import com.tngtech.archunit.lang.ArchRule
import com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses
import com.tngtech.archunit.library.GeneralCodingRules.NO_CLASSES_SHOULD_ACCESS_STANDARD_STREAMS
import com.tngtech.archunit.library.GeneralCodingRules.NO_CLASSES_SHOULD_THROW_GENERIC_EXCEPTIONS
import com.tngtech.archunit.library.GeneralCodingRules.NO_CLASSES_SHOULD_USE_FIELD_INJECTION
import com.tngtech.archunit.library.GeneralCodingRules.NO_CLASSES_SHOULD_USE_JAVA_UTIL_LOGGING
import com.tngtech.archunit.library.GeneralCodingRules.NO_CLASSES_SHOULD_USE_JODATIME

@AnalyzeClasses(
  packages = ["com.stemmildt.discover"],
  importOptions = [DoNotIncludeTests::class, DoNotIncludeArchives::class]
)
class GeneralCodingRulesTest {

  @ArchTest
  val noClassesShouldAccessStandardStreams: ArchRule = NO_CLASSES_SHOULD_ACCESS_STANDARD_STREAMS

  @ArchTest
  val noClassesShouldThrowGenericExceptions: ArchRule = NO_CLASSES_SHOULD_THROW_GENERIC_EXCEPTIONS

  @ArchTest
  val noClassesShouldUseStandardLogging: ArchRule = NO_CLASSES_SHOULD_USE_JAVA_UTIL_LOGGING

  @ArchTest
  val noClassesShouldUseJodaTime: ArchRule = NO_CLASSES_SHOULD_USE_JODATIME

  @ArchTest
  val noClassesShouldUseFieldInjection: ArchRule = NO_CLASSES_SHOULD_USE_FIELD_INJECTION

  @ArchTest
  val noClassesShouldBeSuffixedWithImpl: ArchRule =
    noClasses()
      .should().haveSimpleNameEndingWith("Impl")
      .because("seriously, you can do better than that")
}
