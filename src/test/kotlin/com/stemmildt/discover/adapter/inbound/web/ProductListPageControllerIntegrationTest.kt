package com.stemmildt.discover.adapter.inbound.web

import arrow.core.andThen
import com.stemmildt.discover.IntegrationTest
import com.stemmildt.discover.IntegrationTest.Companion.serviceContext
import com.stemmildt.discover.IntegrationTest.Companion.webTestClient
import com.stemmildt.discover.adapter.inbound.InboundAdapterConfiguration.DISCOVER_PATH
import com.stemmildt.discover.adapter.inbound.web.ProductListPageController.Companion.PRODUCTS_PATH
import com.stemmildt.discover.application.ProductSavedEventApplicationService
import com.stemmildt.discover.domain.model.messaging.ProductSavedEvent
import com.stemmildt.discover.domain.model.product.`object`.Product
import com.stemmildt.discover.domain.model.product.`object`.ProductDescription
import com.stemmildt.discover.domain.model.product.`object`.ProductId
import com.stemmildt.extensions.Validation.validate
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import org.springframework.http.MediaType.TEXT_HTML
import strikt.api.expectThat
import strikt.assertions.contains

class ProductListPageControllerIntegrationTest : IntegrationTest {

  private val productSavedEventApplicationService = serviceContext.getBean(ProductSavedEventApplicationService::class.java)

  @Test
  fun expectToGetProductListPage() {
    val productDescriptionValue = "Pizza"

    validate(
      ProductId.of(1),
      ProductDescription.of(productDescriptionValue)
    ).andThen { (id, description) ->
      Product.of(id, description).andThen(ProductSavedEvent::of)
    }.tap {
      runBlocking { productSavedEventApplicationService.handle(it) }
    }

    webTestClient.get()
      .uri(DISCOVER_PATH + PRODUCTS_PATH)
      .accept(TEXT_HTML)
      .exchange()
      .expectStatus().isOk
      .expectBody(String::class.java)
      .value {
        expectThat(it)
          .contains(productDescriptionValue)
      }
  }
}
