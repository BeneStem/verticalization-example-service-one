package com.stemmildt.discover.adapter.inbound.web

import arrow.core.validNel
import com.stemmildt.discover.adapter.inbound.InboundAdapterConfiguration.DISCOVER_PATH
import com.stemmildt.discover.adapter.inbound.web.ProductListPageController.Companion.PRODUCTS_PATH
import com.stemmildt.discover.application.port.inbound.ProductApplicationService
import com.stemmildt.discover.domain.model.product.`object`.Product
import com.stemmildt.discover.domain.model.product.`object`.ProductDescription
import com.stemmildt.discover.domain.model.product.`object`.ProductId
import gg.jte.ContentType
import gg.jte.TemplateEngine
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.reactive.asFlow
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import org.springframework.http.MediaType.TEXT_HTML
import org.springframework.test.web.reactive.server.WebTestClient
import reactor.kotlin.core.publisher.toFlux
import strikt.api.expectThat
import strikt.assertions.contains

class ProductListPageControllerUnitTest {

  private val productApplicationServiceMock = mockk<ProductApplicationService>()
  private val productListPageController = ProductListPageController(TemplateEngine.createPrecompiled(ContentType.Html), productApplicationServiceMock)

  private val webTestClient = WebTestClient.bindToRouterFunction(productListPageController.routes()).build()

  @Test
  fun expectToGetProductListPage() {
    val productDescriptionValue = "Pizza"

    val pizza = Product(ProductId(1), ProductDescription(productDescriptionValue))
    every { runBlocking { productApplicationServiceMock.loadProductCount() } } returns 1L
    every { runBlocking { productApplicationServiceMock.loadAllProducts() } } returns listOf(pizza.validNel()).toFlux().asFlow()

    webTestClient.get()
      .uri(DISCOVER_PATH + PRODUCTS_PATH)
      .accept(TEXT_HTML)
      .exchange()
      .expectStatus().isOk
      .expectBody(String::class.java)
      .value {
        expectThat(it)
          .contains(productDescriptionValue)
      }
  }
}
