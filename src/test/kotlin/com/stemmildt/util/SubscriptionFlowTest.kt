package com.stemmildt.util

import kotlinx.coroutines.flow.take
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo

class SubscriptionFlowTest {

  @Test
  fun expectToSendAndGetElements() {
    val subscriptionFlow = SubscriptionFlow<Int>()
    val flow = subscriptionFlow.getFlowFromHere()

    runBlocking {
      subscriptionFlow.send(1)
      subscriptionFlow.send(2)
      subscriptionFlow.send(3)
    }

    expectThat(runBlocking { flow.take(3).toList() })
      .isEqualTo(listOf(1, 2, 3))
  }
}
