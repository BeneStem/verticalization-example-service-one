package com.stemmildt.discover

import com.stemmildt.discover.DiscoverServiceConfiguration.discoverServiceApplication

object DiscoverService

fun main(args: Array<String>) {
  discoverServiceApplication.run(args)
}
