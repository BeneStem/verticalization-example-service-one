package com.stemmildt.discover.adapter.outbound

import com.stemmildt.discover.adapter.outbound.messaging.flow.FlowProductSavedEventForwarder
import com.stemmildt.discover.adapter.outbound.persistence.mongo.MongoProductRepository
import org.springframework.boot.autoconfigure.mongo.MongoProperties
import org.springframework.core.env.getProperty
import org.springframework.fu.kofu.configuration
import org.springframework.fu.kofu.mongo.reactiveMongodb

object OutboundAdapterConfiguration {

  operator fun invoke() = configuration {
    enable(persistenceConfiguration())
    enable(messagingConfiguration())
  }

  private fun persistenceConfiguration() = configuration {
    reactiveMongodb {
      uri = env.getProperty<String>("spring.data.mongodb.uri") ?: MongoProperties.DEFAULT_URI
    }
    beans {
      bean<MongoProductRepository>()
    }
  }

  private fun messagingConfiguration() = configuration {
    beans {
      bean<FlowProductSavedEventForwarder>()
    }
  }
}
