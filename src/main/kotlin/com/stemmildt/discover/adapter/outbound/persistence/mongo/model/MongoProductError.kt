package com.stemmildt.discover.adapter.outbound.persistence.mongo.model

import com.stemmildt.util.Error

sealed class MongoProductError : Error {

  data object DuplicateKey : MongoProductError()
  data object IdNonInteger : MongoProductError()
  data object DescriptionNull : MongoProductError()
}
