package com.stemmildt.discover.adapter.outbound.persistence.mongo

import arrow.core.invalidNel
import com.stemmildt.discover.adapter.outbound.persistence.mongo.model.MongoProduct
import com.stemmildt.discover.adapter.outbound.persistence.mongo.model.MongoProduct.Companion.toMongoProduct
import com.stemmildt.discover.adapter.outbound.persistence.mongo.model.MongoProductError
import com.stemmildt.discover.application.port.outbound.ProductRepository
import com.stemmildt.discover.domain.model.product.`object`.Product
import kotlinx.coroutines.flow.map
import org.springframework.dao.DuplicateKeyException
import org.springframework.data.mongodb.core.ReactiveFluentMongoOperations
import org.springframework.data.mongodb.core.awaitCount
import org.springframework.data.mongodb.core.flow
import org.springframework.data.mongodb.core.insert
import org.springframework.data.mongodb.core.oneAndAwait
import org.springframework.data.mongodb.core.query

class MongoProductRepository(
  private val mongo: ReactiveFluentMongoOperations
) : ProductRepository {

  override suspend fun findAllProducts() =
    mongo.query<MongoProduct>()
      .flow()
      .map { it.toProduct() }

  override suspend fun countProducts() =
    mongo.query<MongoProduct>()
      .awaitCount()

  override suspend fun persistProduct(product: Product) =
    try {
      mongo.insert<MongoProduct>()
        .oneAndAwait(product.toMongoProduct())
        .toProduct()
    } catch (duplicateKeyException: DuplicateKeyException) {
      MongoProductError.DuplicateKey.invalidNel()
    }
}
