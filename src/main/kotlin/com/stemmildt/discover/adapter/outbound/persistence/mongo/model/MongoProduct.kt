package com.stemmildt.discover.adapter.outbound.persistence.mongo.model

import arrow.core.andThen
import arrow.core.invalidNel
import arrow.core.validNel
import com.stemmildt.discover.adapter.outbound.persistence.mongo.model.MongoProductError.DescriptionNull
import com.stemmildt.discover.adapter.outbound.persistence.mongo.model.MongoProductError.IdNonInteger
import com.stemmildt.discover.domain.model.product.`object`.Product
import com.stemmildt.discover.domain.model.product.`object`.ProductDescription
import com.stemmildt.discover.domain.model.product.`object`.ProductId
import com.stemmildt.extensions.Validation.validate
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document

@Document(collection = "products")
data class MongoProduct(
  @Id
  val id: String?,
  val description: String?
) {

  companion object {

    fun Product.toMongoProduct() = MongoProduct(id.value.toString(), description.value)
  }

  private fun validateId() = id?.toIntOrNull()?.validNel()
    ?: IdNonInteger.invalidNel()

  private fun validateDescription() = description?.validNel()
    ?: DescriptionNull.invalidNel()

  fun toProduct() =
    validate(
      validateId().andThen(ProductId::of),
      validateDescription().andThen(ProductDescription::of)
    ).andThen { (id, description) ->
      Product.of(id, description)
    }
}
