package com.stemmildt.discover.adapter.outbound.messaging.flow

import com.stemmildt.discover.application.port.outbound.ProductSavedEventForwarder
import com.stemmildt.discover.application.port.outbound.ProductSavedEventSubscriber
import com.stemmildt.discover.domain.model.messaging.ProductSavedEvent
import com.stemmildt.util.SubscriptionFlow

class FlowProductSavedEventForwarder : ProductSavedEventForwarder, ProductSavedEventSubscriber {

  private val subscriptionFlow = SubscriptionFlow<ProductSavedEvent>()

  override fun forward(productSavedEvent: ProductSavedEvent) = subscriptionFlow.send(productSavedEvent)

  override fun subscribe() = subscriptionFlow.getFlowFromHere()
}
