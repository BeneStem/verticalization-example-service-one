package com.stemmildt.discover.adapter.inbound.web

import com.stemmildt.discover.adapter.inbound.InboundAdapterConfiguration.DISCOVER_PATH
import com.stemmildt.extensions.EntityResponse.buildAndAwait
import com.stemmildt.extensions.EntityResponse.ok
import gg.jte.TemplateEngine
import gg.jte.output.StringOutput
import org.springframework.http.MediaType.TEXT_HTML
import org.springframework.web.reactive.function.server.EntityResponse
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.coRouter

class SsiController(
  private val templateEngine: TemplateEngine
) {

  fun routes() = coRouter {
    accept(TEXT_HTML).and(path("/ssi")).nest {
      GET("$DISCOVER_PATH/header", ::getHeaderSsi)
      GET("$DISCOVER_PATH/footer", ::getFooterSsi)
    }
  }

  @Suppress("UNUSED_PARAMETER")
  private suspend fun getHeaderSsi(serverRequest: ServerRequest): EntityResponse<String> =
    StringOutput().let {
      templateEngine.render("ssi/header", emptyMap(), it)
      ok(it.toString()).contentType(TEXT_HTML).buildAndAwait()
    }

  @Suppress("UNUSED_PARAMETER")
  private suspend fun getFooterSsi(serverRequest: ServerRequest): EntityResponse<String> =
    StringOutput().let {
      templateEngine.render("ssi/footer", emptyMap(), it)
      ok(it.toString()).contentType(TEXT_HTML).buildAndAwait()
    }
}
