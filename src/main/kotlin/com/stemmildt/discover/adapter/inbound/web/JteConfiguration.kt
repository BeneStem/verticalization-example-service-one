package com.stemmildt.discover.adapter.inbound.web

import gg.jte.ContentType
import gg.jte.TemplateEngine

object JteConfiguration {

  fun templateEngine(): TemplateEngine = TemplateEngine.createPrecompiled(ContentType.Html)
}
