package com.stemmildt.discover.adapter.inbound.web

import com.stemmildt.discover.adapter.inbound.InboundAdapterConfiguration.DISCOVER_PATH
import com.stemmildt.discover.adapter.inbound.web.ProductListPageController.Companion.PRODUCTS_PATH
import com.stemmildt.discover.application.port.inbound.ProductApplicationService
import com.stemmildt.extensions.EntityResponse.buildAndAwait
import com.stemmildt.extensions.EntityResponse.ok
import com.stemmildt.util.TextEventStream.toTextEventStreamDataString
import gg.jte.TemplateEngine
import gg.jte.output.StringOutput
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import org.springframework.http.MediaType.TEXT_EVENT_STREAM
import org.springframework.web.reactive.function.server.EntityResponse
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.coRouter

class ProductCountController(
  private val templateEngine: TemplateEngine,
  private val productApplicationService: ProductApplicationService
) {

  fun routes() = coRouter {
    accept(TEXT_EVENT_STREAM).nest {
      GET("$DISCOVER_PATH$PRODUCTS_PATH/count", ::getProductCount)
    }
  }

  @Suppress("UNUSED_PARAMETER")
  private suspend fun getProductCount(serverRequest: ServerRequest): EntityResponse<Flow<String>> =
    productApplicationService.streamProductCount()
      .map {
        StringOutput().let { output ->
          templateEngine.render("fragments/productCount", mapOf("productCount" to it), output)
          output.toTextEventStreamDataString()
        }
      }
      .let {
        ok(it).contentType(TEXT_EVENT_STREAM).buildAndAwait()
      }
}
