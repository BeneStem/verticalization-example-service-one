package com.stemmildt.discover.adapter.inbound.messaging

import com.stemmildt.create.v1.KafkaProduct
import com.stemmildt.discover.adapter.inbound.messaging.KafkaConsumerConfiguration.PRODUCTS_TOPIC
import com.stemmildt.discover.adapter.inbound.messaging.model.KafkaProductEvents.toProductSavedEvent
import com.stemmildt.discover.application.port.inbound.ProductSavedEventApplicationService
import com.stemmildt.shared.v1.Envelope
import com.stemmildt.shared.v1.Envelope.Event.Header.Type.SAVED
import mu.KLogging
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory
import org.springframework.kafka.listener.MessageListener

// TODO NEXT use KafkaErrors

class KafkaProductSavedEventListener(
  private val listenerFactory: ConcurrentKafkaListenerContainerFactory<String, ByteArray>,
  private val productSavedEventApplicationService: ProductSavedEventApplicationService
) {

  companion object : KLogging()

  fun create() =
    listenerFactory.createContainer(PRODUCTS_TOPIC).also {
      it.setupMessageListener(
        MessageListener<String, ByteArray> { listen(it.value()) }
      )
    }

  private fun listen(message: ByteArray) =
    Envelope.Event.parseFrom(message).let {
      when (val type = it.header.type) {
        SAVED -> handleKafkaProductSavedEvent(it)
        else -> logger.error { "Unknown KafkaProductEvent type: ${type.name}" }
      }
    }

  private fun handleKafkaProductSavedEvent(event: Envelope.Event) =
    when (val version = event.header.version) {
      1 -> KafkaProduct.Saved.parseFrom(event.body).toProductSavedEvent()
        .fold(
          { logger.error { "Invalid KafkaProductSavedEvent: $it" } },
          { productSavedEventApplicationService.handle(it) }
        )
      else -> logger.error { "Unknown KafkaProductSavedEvent version: $version" }
    }
}
