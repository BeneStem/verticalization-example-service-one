package com.stemmildt.discover.adapter.inbound.web

import arrow.core.orNull
import com.stemmildt.discover.adapter.inbound.InboundAdapterConfiguration.DISCOVER_PATH
import com.stemmildt.discover.application.port.inbound.ProductApplicationService
import com.stemmildt.discover.domain.model.web.ProductList
import com.stemmildt.discover.domain.model.web.ProductTile
import com.stemmildt.extensions.EntityResponse.buildAndAwait
import com.stemmildt.extensions.EntityResponse.ok
import gg.jte.Content
import gg.jte.TemplateEngine
import gg.jte.output.StringOutput
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.toList
import org.springframework.http.MediaType.TEXT_HTML
import org.springframework.web.reactive.function.server.EntityResponse
import org.springframework.web.reactive.function.server.ServerRequest
import org.springframework.web.reactive.function.server.coRouter

class ProductListPageController(
  private val templateEngine: TemplateEngine,
  private val productApplicationService: ProductApplicationService
) {

  companion object {

    const val PRODUCTS_PATH: String = "/products"
  }

  fun routes() = coRouter {
    accept(TEXT_HTML).nest {
      GET(DISCOVER_PATH + PRODUCTS_PATH, ::getProductListPage)
    }
  }

  @Suppress("UNUSED_PARAMETER")
  private suspend fun getProductListPage(serverRequest: ServerRequest): EntityResponse<String> =
    StringOutput().let {
      val mainMap = mapOf("productList" to ProductList(productApplicationService.loadProductCount(), findProductTiles()))
      templateEngine.render(
        "templates/pageFrame",
        mapOf(
          "head" to Content { templateEngine.render("fragments/head", mapOf("title" to "ProductListPage"), it) },
          "main" to Content { templateEngine.render("fragments/productList", mainMap, it) }
        ),
        it
      )
      ok(it.toString()).contentType(TEXT_HTML).buildAndAwait()
    }

  private suspend fun findProductTiles() =
    productApplicationService.loadAllProducts()
      .map { it.map { ProductTile(it) }.orNull() }
      .filterNotNull()
      .toList()
}
