package com.stemmildt.discover.adapter.inbound

import com.stemmildt.discover.adapter.inbound.messaging.KafkaConsumerConfiguration
import com.stemmildt.discover.adapter.inbound.messaging.KafkaProductSavedEventListener
import com.stemmildt.discover.adapter.inbound.web.JteConfiguration
import com.stemmildt.discover.adapter.inbound.web.ProductCountController
import com.stemmildt.discover.adapter.inbound.web.ProductListPageController
import com.stemmildt.discover.adapter.inbound.web.ProductRecommendationsSsiController
import com.stemmildt.discover.adapter.inbound.web.SsiController
import org.springframework.boot.autoconfigure.kafka.KafkaProperties
import org.springframework.core.env.getRequiredProperty
import org.springframework.fu.kofu.configuration
import org.springframework.fu.kofu.webflux.webFlux

object InboundAdapterConfiguration {

  const val DISCOVER_PATH: String = "/discover"

  operator fun invoke() = configuration {
    enable(webConfiguration())
    enable(messagingConfiguration())
  }

  private fun webConfiguration() = configuration {
    webFlux {
      port = env.getRequiredProperty<Int>("server.port")
      codecs {
        string()
      }
    }
    beans {
      bean(JteConfiguration::templateEngine)

      bean<ProductListPageController>()
      bean(ProductListPageController::routes)

      bean<ProductRecommendationsSsiController>()
      bean(ProductRecommendationsSsiController::routes)

      bean<SsiController>()
      bean(SsiController::routes)

      bean<ProductCountController>()
      bean(ProductCountController::routes)
    }
  }

  private fun messagingConfiguration() = configuration {
    beans {
      configurationProperties<KafkaProperties>(prefix = "spring.kafka")
      bean<KafkaConsumerConfiguration>()
      bean(KafkaConsumerConfiguration::listenerFactory)
      bean<KafkaProductSavedEventListener>()
      bean(KafkaProductSavedEventListener::create)
    }
  }
}
