package com.stemmildt.discover.application

import com.stemmildt.discover.application.port.inbound.ProductApplicationService
import com.stemmildt.discover.application.port.outbound.ProductRepository
import com.stemmildt.discover.application.port.outbound.ProductSavedEventSubscriber
import kotlinx.coroutines.FlowPreview
import kotlinx.coroutines.flow.flatMapConcat
import kotlinx.coroutines.flow.flowOf

class ProductApplicationService(
  private val productRepository: ProductRepository,
  private val productSavedEventSubscriber: ProductSavedEventSubscriber
) : ProductApplicationService {

  override suspend fun loadAllProducts() =
    productRepository.findAllProducts()

  @OptIn(FlowPreview::class)
  override suspend fun streamProductCount() =
    productSavedEventSubscriber.subscribe()
      .flatMapConcat { flowOf(productRepository.countProducts()) }

  override suspend fun loadProductCount() =
    productRepository.countProducts()
}
