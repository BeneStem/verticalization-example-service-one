package com.stemmildt.discover.application.port.inbound

import arrow.core.ValidatedNel
import com.stemmildt.discover.domain.model.product.`object`.Product
import com.stemmildt.util.Error
import kotlinx.coroutines.flow.Flow

interface ProductApplicationService {

  suspend fun loadAllProducts(): Flow<ValidatedNel<Error, Product>>

  suspend fun streamProductCount(): Flow<Long>

  suspend fun loadProductCount(): Long
}
