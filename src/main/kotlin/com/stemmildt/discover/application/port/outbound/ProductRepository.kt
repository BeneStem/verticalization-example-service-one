package com.stemmildt.discover.application.port.outbound

import arrow.core.ValidatedNel
import com.stemmildt.discover.domain.model.product.`object`.Product
import com.stemmildt.util.Error
import kotlinx.coroutines.flow.Flow

interface ProductRepository {

  suspend fun findAllProducts(): Flow<ValidatedNel<Error, Product>>

  suspend fun countProducts(): Long

  suspend fun persistProduct(product: Product): ValidatedNel<Error, Product>
}
