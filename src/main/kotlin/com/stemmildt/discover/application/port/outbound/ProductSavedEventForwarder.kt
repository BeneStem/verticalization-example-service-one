package com.stemmildt.discover.application.port.outbound

import com.stemmildt.discover.domain.model.messaging.ProductSavedEvent

interface ProductSavedEventForwarder {

  fun forward(productSavedEvent: ProductSavedEvent)
}
