package com.stemmildt.discover.application.port.outbound

import com.stemmildt.discover.domain.model.messaging.ProductSavedEvent
import kotlinx.coroutines.flow.Flow

interface ProductSavedEventSubscriber {

  fun subscribe(): Flow<ProductSavedEvent>
}
