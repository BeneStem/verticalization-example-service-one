package com.stemmildt.discover.application.port.inbound

import com.stemmildt.discover.domain.model.messaging.ProductSavedEvent

interface ProductSavedEventApplicationService {

  fun handle(productSavedEvent: ProductSavedEvent)
}
