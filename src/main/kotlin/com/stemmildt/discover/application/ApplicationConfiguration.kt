package com.stemmildt.discover.application

import org.springframework.fu.kofu.configuration

object ApplicationConfiguration {

  operator fun invoke() = configuration {
    beans {
      bean<ProductApplicationService>()
      bean<ProductSavedEventApplicationService>()
    }
  }
}
