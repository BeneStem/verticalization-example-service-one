package com.stemmildt.discover.application

import com.stemmildt.discover.application.port.inbound.ProductSavedEventApplicationService
import com.stemmildt.discover.application.port.outbound.ProductRepository
import com.stemmildt.discover.application.port.outbound.ProductSavedEventForwarder
import com.stemmildt.discover.domain.model.messaging.ProductSavedEvent
import kotlinx.coroutines.runBlocking

class ProductSavedEventApplicationService(
  private val productRepository: ProductRepository,
  private val productSavedEventForwarder: ProductSavedEventForwarder
) : ProductSavedEventApplicationService {

  override fun handle(productSavedEvent: ProductSavedEvent) =
    runBlocking {
      productRepository.persistProduct(productSavedEvent.product).tap {
        productSavedEventForwarder.forward(productSavedEvent)
      }
      Unit
    }
}
