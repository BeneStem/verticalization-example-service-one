package com.stemmildt.discover.domain.model.product.`object`

import arrow.core.validNel
import com.stemmildt.extensions.Validation.ValidationException
import com.stemmildt.extensions.Validation.get
import org.jmolecules.ddd.annotation.AggregateRoot

// TODO NOW split backoffice & frontoffice, name domain models and move packages/classes/urls accordingly

@AggregateRoot
class Product private constructor(
  val id: ProductId,
  val description: ProductDescription
) {

  companion object {
    fun of(id: ProductId, description: ProductDescription) =
      Product(id, description).validNel()

    @Throws(ValidationException::class)
    operator fun invoke(id: ProductId, description: ProductDescription) =
      of(id, description).get()
  }

  override fun equals(other: Any?): Boolean {
    if (this === other) return true
    if (other !is Product) return false

    if (id != other.id) return false

    return true
  }

  override fun hashCode() = id.hashCode()

  override fun toString() = "Product(id=$id, description=$description)"
}
