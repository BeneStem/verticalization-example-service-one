package com.stemmildt.discover.domain.model.web

import org.jmolecules.ddd.annotation.AggregateRoot

// TODO NOW this domain structure is not optimal

@AggregateRoot
class ProductList(
  val productCount: Long,
  val productTiles: List<ProductTile>
) {

  companion object {

    operator fun invoke(productCount: Long, productTiles: List<ProductTile>) = ProductList(productCount, productTiles)
  }

  override fun equals(other: Any?): Boolean {
    if (this === other) return true
    if (other !is ProductList) return false

    if (productCount != other.productCount) return false
    if (productTiles != other.productTiles) return false

    return true
  }

  override fun hashCode(): Int {
    var result = productCount.hashCode()
    result = 31 * result + productTiles.hashCode()
    return result
  }

  override fun toString() = "ProductList(productCount=$productCount, productTiles=$productTiles)"
}
