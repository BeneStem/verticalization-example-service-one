package com.stemmildt.discover.domain.model.web

import com.stemmildt.discover.adapter.inbound.web.ProductListPageController.Companion.PRODUCTS_PATH
import com.stemmildt.discover.domain.model.product.`object`.Product
import org.jmolecules.ddd.annotation.ValueObject

// TODO NOW handle error states

@ValueObject
data class ProductTile private constructor(
  val headline: String,
  val text: String,
  val link: String
) {

  companion object {

    operator fun invoke(product: Product) =
      ProductTile(
        product.description.value,
        product.id.value.toString(),
        // TODO NOW huge ddd architecture violation (!!)
        "/evaluate$PRODUCTS_PATH/${product.id.value}"
      )
  }
}
