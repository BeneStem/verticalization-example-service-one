package com.stemmildt.discover.domain.model.product.`object`

import arrow.core.ValidatedNel
import arrow.core.invalidNel
import arrow.core.validNel
import com.stemmildt.discover.domain.model.product.`object`.ProductError.DescriptionInvalid
import com.stemmildt.extensions.Validation.ValidationException
import com.stemmildt.extensions.Validation.get
import com.stemmildt.util.Sanitizing.sanitize
import org.jmolecules.ddd.annotation.ValueObject

@ValueObject
@JvmInline
value class ProductDescription private constructor(val value: String) {

  companion object {

    @Throws(ValidationException::class)
    operator fun invoke(value: String) =
      of(value).get()

    fun of(value: String): ValidatedNel<ProductError, ProductDescription> =
      value.sanitize().let {
        if (it != "" && it.length <= 200) {
          ProductDescription(it).validNel()
        } else {
          DescriptionInvalid.invalidNel()
        }
      }
  }
}
