package com.stemmildt.discover

import com.stemmildt.discover.adapter.outbound.OutboundAdapterConfiguration
import com.stemmildt.discover.application.ApplicationConfiguration
import org.springframework.fu.kofu.reactiveWebApplication

object DiscoverServiceConfiguration {

  val discoverServiceApplication = reactiveWebApplication {
    enable(com.stemmildt.discover.adapter.inbound.InboundAdapterConfiguration())
    enable(OutboundAdapterConfiguration())
    enable(ApplicationConfiguration())
  }
}
