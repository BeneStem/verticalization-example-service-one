package com.stemmildt.extensions

import arrow.core.None
import arrow.core.Option
import arrow.core.Some

object Option {

  fun <A> some(value: A): Option<A> = Some(value)
  fun <A> none(): Option<A> = None

  fun <A> Option<A>.get(): A =
    when (this) {
      is None -> throw AssertionError("found None where Some was expected")
      is Some -> value
    }
}
